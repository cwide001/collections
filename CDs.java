class CDs {

    private String title;
    private String artist;

    public CDs(String title, String artist) {
        this.title = title;
        this.artist = artist;
    }

    public String toString() {
        return "Title: " + title + " Artist: " + artist;
    }
}