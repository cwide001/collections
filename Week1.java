import java.util.*;
class CollectionTypes {

    public static void main(String[] args) {

        System.out.println("-- List --");
        List list = new ArrayList();
        list.add("This");
        list.add("is");
        list.add("new");
        list.add("for");
        list.add("me");
        list.add("HELP!!");

        for (Object str : list) {
            System.out.println((String)str);
        }

        System.out.println("-- Set --");
        Set set = new TreeSet();
        set.add("This");
        set.add("is");
        set.add("new");
        set.add("for");
        set.add("me");
        set.add("HELP!!");

        for (Object str : set) {
            System.out.println((String)str);
        }

        System.out.println("-- Queue --");
        Queue queue = new PriorityQueue();
        queue.add("This");
        queue.add("is");
        queue.add("new");
        queue.add("for");
        queue.add("me");
        queue.add("HELP!!");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("-- Map --");
        Map map = new HashMap();
        map.put(1,"This");
        map.put(2,"is");
        map.put(3,"new");
        map.put(4,"for");
        map.put(5,"me");
        map.put(4,"HELP");

        for (int i = 1; i < 6; i++) {
            String result = (String)map.get(i);
            System.out.println(result);
        }

        System.out.println("-- List using Generics --");
        List<CDs> myList = new LinkedList<CDs>();
        myList.add(new CDs("EVERMORE", "Taylor Swift"));
        myList.add(new CDs("LOVE GOES", "Sam Smith"));
        myList.add(new CDs("50 - YEARS DONT STOP", "Fleetwood Mac"));
        myList.add(new CDs("DIAMONDS", "Elton John"));

        for (CDs cd : myList) {
            System.out.println(cd);
        }

    }
} 